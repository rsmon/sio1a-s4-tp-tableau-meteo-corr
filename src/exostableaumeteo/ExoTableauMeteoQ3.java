package exostableaumeteo;

public class ExoTableauMeteoQ3 {

    static int [][]  tableTemperatures = Data.lesTemperatures;
       
    static String[]  tableMois         = Data.lesMois;
      
    
    public static void main(String[] args) {
    
        //<editor-fold desc="Q3 Afficher les jours de l'année où il a fait 25°C">
        
        System.out.println("\nIl a fait  25°C aux dates suivantes:\n");
          
        for ( int m=0; m<tableTemperatures.length;m++){
        
          for ( int j=0; j< tableTemperatures[m].length;j++){
                    
             if ( tableTemperatures[m][j]==25 ){
                 
                 System.out.printf("%2d %-15s\n",j+1,tableMois[m]);                       
             }
          }
        }
          
        System.out.println();
        
        //</editor-fold>
    }
}




