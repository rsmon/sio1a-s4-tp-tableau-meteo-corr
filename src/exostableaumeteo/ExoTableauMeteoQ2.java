package exostableaumeteo;

public class ExoTableauMeteoQ2 {

   static int [][]  tableTemperatures = Data.lesTemperatures;    
   static String[]  tableMois         = Data.lesMois;
    
    public static void main(String[] args) {
    
        //<editor-fold desc="Q2: Afficher la température relevée le 10 Mars">
        
        System.out.println();
          
        System.out.printf("Température relevée le 10 Mars: %2d°C\n",tableTemperatures[2][9]);
        
        System.out.println();
        
        //</editor-fold>
      
    }
}




