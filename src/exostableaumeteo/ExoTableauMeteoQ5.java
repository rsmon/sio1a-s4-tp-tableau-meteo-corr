package exostableaumeteo;

public class ExoTableauMeteoQ5 {

    static int [][]  tableTemperatures = Data.lesTemperatures;
       
    static String[]  tableMois         = Data.lesMois;
        
    public static void main(String[] args) {
    
        //<editor-fold desc="Q6: Afficher pour chaque les températures  Moyenne, Mini et Maxi">
        
      
         System.out.println("Températures  Moyenne, Minimale et maximale de chaque mois\n");
         
         for ( int m=0; m<tableTemperatures.length;m++){
       
             
           int totalTemp=0, tempMini=99,tempMaxi=-99; 
           
           float tempMoyenne=0;
           
           for ( int j=0; j< tableTemperatures[m].length;j++){
                    
              totalTemp+=tableTemperatures[m][j];
           
              if(tableTemperatures[m][j]< tempMini)tempMini=tableTemperatures[m][j];
              if(tableTemperatures[m][j]> tempMaxi)tempMaxi=tableTemperatures[m][j];
           }
           
           tempMoyenne=(float)totalTemp/tableTemperatures[m].length;
           
           System.out.printf("%-11s Moyenne = %4.1f° Minimale = %2d° Maximale = %2d°\n",
                              tableMois[m],tempMoyenne,tempMini,tempMaxi
                            );
              
         }
          
        System.out.println();
        
        //</editor-fold>
      
    }

    private static void afficherEnteteJours() {
        System.out.println("Tableau annuel des températures\n");
        System.out.printf("%-11s"," ");
        for(int j=1;j<=31;j++){ System.out.printf("%2d  ",j);}
        System.out.println();
    }
}




