package exostableaumeteo;

public class ExoTableauMeteoQ6 {

    static int [][]  tableTemperatures = Data.lesTemperatures;       
    static String[]  tableMois         = Data.lesMois;
    
    public static void main(String[] args) {
    
        //<editor-fold desc="Q7: Afficher la température maximale de l'année ainsi que les jours où elle a été atteinte">
          
         int tempMaxi=-99; 
         for ( int m=0; m<tableTemperatures.length;m++){
    
           for ( int j=0; j< tableTemperatures[m].length;j++){
                    
              if(tableTemperatures[m][j]> tempMaxi)tempMaxi=tableTemperatures[m][j];
              
           }
                
         }
          
         System.out.printf("\nTempérature Maximale de l'année: %2d°\n",tempMaxi);
         
         
         System.out.println("\nElle a été atteinte au dates suivantes:\n");
         
         
         for ( int m=0; m<tableTemperatures.length;m++){
    
           for ( int j=0; j< tableTemperatures[m].length;j++){
                    
              if(tableTemperatures[m][j]== tempMaxi){
                  
                  System.out.printf("%2d %-10s\n",j+1,tableMois[m]);
                  
              }
              
           }
                
         }
         
         
         
         System.out.println();
        
        //</editor-fold>
      
    }

    
}




