package exostableaumeteo;

public class ExoTableauMeteoQ4 {

    
 static int [][]  tableTemperatures = Data.lesTemperatures;
       
    static String[]  tableMois         = Data.lesMois;
    public static void main(String[] args) {
    
        //<editor-fold desc="Q4 Afficher les jours de l'année où il a gelé">
        
        System.out.println("\nIl a gelé aux dates suivantes:\n");
          
        for ( int m=0; m<tableTemperatures.length;m++){
        
          for ( int j=0; j< tableTemperatures[m].length;j++){
              
             if ( tableTemperatures[m][j] <= 0 ){
                 
                 System.out.printf("%2d %-10s %2d°\n",j+1,tableMois[m],tableTemperatures[m][j]);                       
             }
          }
        }
          
        System.out.println();
        
        //</editor-fold>
    }
}




