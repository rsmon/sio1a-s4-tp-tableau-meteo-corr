package exostableaumeteo;

public class ExoTableauMeteoQ7 {

    static int [][]  tableTemperatures = Data.lesTemperatures;       
    static String[]  tableMois         = Data.lesMois;
     
    public static void main(String[] args) {
    
        //<editor-fold desc="Q5: Afficher le tableau annuel des températures">
        
         
         afficherEnteteJours();
         
         System.out.println();
         
         for ( int m=0; m<tableTemperatures.length;m++){
        
           System.out.printf("%-11s",tableMois[m]);   
             
           for ( int j=0; j< tableTemperatures[m].length;j++){
                    
               System.out.printf("%2d° ",tableTemperatures[m][j]);
           }
           System.out.println(); 
         }
          
        System.out.println();
        
        //</editor-fold>
      
    }

    private static void afficherEnteteJours() {
        System.out.println("Tableau annuel des températures\n");
        System.out.printf("%-11s"," ");
        for(int j=1;j<=31;j++){ System.out.printf("%2d  ",j);}
        System.out.println();
    }
}




