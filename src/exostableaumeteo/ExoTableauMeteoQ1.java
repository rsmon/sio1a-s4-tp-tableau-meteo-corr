package exostableaumeteo;

public class ExoTableauMeteoQ1 {

    static int [][]  tableTemperatures = Data.lesTemperatures;       
    static String[]  tableMois         = Data.lesMois;
    
    public static void main(String[] args) {
    
        //<editor-fold desc="Q1: Afficher le nom de chaque mois avec le nombre de jours qu'il comporte">
        
        System.out.println();
        
        for(int i= 0; i<tableMois.length;i++){
          
              System.out.printf("%-15s %2d Jours\n",tableMois[i],tableTemperatures[i].length);
        }
        
        System.out.println();
        
        //</editor-fold>
      
    }
}




